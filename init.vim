set nocompatible              " be iMproved, required
filetype off                  " required
filetype plugin indent on    " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'kien/rainbow_parentheses.vim'
Plugin 'jiangmiao/auto-pairs'
"bugged
"Plugin 'sheerun/vim-polyglot'
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'Shougo/deoplete.nvim'
Plugin 'zchee/deoplete-clang'
Plugin 'neomake/neomake'
Plugin 'ervandew/supertab'
Plugin 'dylanaraps/wal.vim'
Plugin 'bling/vim-bufferline'
"Plugin 'lervag/vimtex'
Plugin 'vim-latex/vim-latex'
" All of your Plugins must be added before the following line
call vundle#end()            " required

" see :h vundle for more details or wiki for FAQ
syntax on

" set termguicolors
" bufferline shit
let g:bufferline_echo = 1
let g:bufferline_show_bufnr = 1


"colorscheme 
colorscheme wal
set background=dark " for the dark version
let g:one_allow_italics = 1

set mouse=a "enable mouse interaction in all modes
"Identation

" show existing tab with 2 spaces width
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set smartindent
" show white spaces and tabs
":set list lcs=tab:\|\ 
":set listchars=tab:\⎸\ 

"Numbers
set relativenumber
set number

" add prolog syntax support
let g:filetype_pl="prolog"


" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

" use system default clipboard
set clipboard=unnamed

"airline things
set statusline=%t       "tail of the filename
set statusline+=[%{strlen(&fenc)?&fenc:'none'} "file encoding
set statusline+=%{&ff}] "file format
set statusline+=%h      "help file flag
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag
set statusline+=%y      "filetype
set statusline+=%=      "left/right separator
set statusline+=[%P]    "percent through file

:let mapleader = "," 

" tab thought buffer
:nnoremap <Tab> :bnext<CR>
:nnoremap <S-Tab> :bprevious<CR>

" use arrow keys to navigate split windows
nnoremap <leader>k  <C-w>k
nnoremap <leader>j  <C-w>j
nnoremap <leader>h  <C-w>h
nnoremap <leader>l  <C-w>l

let g:SuperTabMappingForward='<s-tab>'
let g:SuperTabMappingBackward='<tab>'

"auto open nerdtree
"autocmd vimenter * NERDTree

"close NERDTree when no windows
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" rainbow_parentheses 
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

"git commands
nnoremap <F1> :Gpull<CR>
nnoremap <F2> :Gwrite<CR>
nnoremap <F3> :Gcommit<CR>
nnoremap <F4> :Gpush<CR>

"use deoplete
let g:deoplete#enable_at_startup = 1

"neomake
call neomake#configure#automake('w')

"autopair temporary `<` issue fix
autocmd VimEnter,BufEnter,BufWinEnter * silent! iunmap <buffer> <M-">
" javacomplete2 enable
autocmd FileType java setlocal omnifunc=javacomplete#Complete
nmap <F6> <Plug>(JavaComplete-Imports-AddMissing)
nnoremap <leader>. :CtrlPTag<cr>
let g:ctrlp_custom_ignore = '\.class$|DS_Store\|git'

":W saves
command WQ wq
command Wq wq
command W w
command Q q

" remote neovim
let g:vimtex_compiler_progname = 'nvr'
